#############################################################################
# File		: Makefile
# Package	: abf_console_client
# Author	: Anton Kirilenko <anton.kirilenko@rosalab.ru>
#############################################################################


PYTHON=python
PYVER := $(shell $(PYTHON) -c 'import sys; print "%.3s" %(sys.version)')
PYSYSDIR := $(shell $(PYTHON) -c 'import sys; print sys.prefix')
PYLIBDIR = $(PYSYSDIR)/lib/python$(PYVER)
PKGDIR = $(PYLIBDIR)/site-packages

BINDIR=/usr/bin
ETCDIR=/etc
MANDIR=/usr/share/man
USRSHAREDIR=/usr/share


FILES = abf/console/*.py abf/*.py abf/api/*.py

all:
	@echo "Nothing to do. Run 'make install' or 'make clean'"

clean:
	rm -f *~ *.pyc *.pyo
	
install:
	mkdir -p $(DESTDIR)$(PKGDIR) $(DESTDIR)$(BINDIR) $(DESTDIR)$(MANDIR)/man1
	cp -p --parents $(FILES) $(DESTDIR)$(PKGDIR)
	cp -p "abf.py" $(DESTDIR)$(BINDIR)/abf
	
	mkdir -p $(DESTDIR)$(USRSHAREDIR)/bash-completion
	mkdir -p $(DESTDIR)$(ETCDIR)/bash_completion.d
	mkdir -p $(DESTDIR)$(ETCDIR)/profile.d
	cp "bash_autocomplete" $(DESTDIR)$(USRSHAREDIR)/bash-completion/abf
	cp "abfcd.sh" $(DESTDIR)$(ETCDIR)/profile.d/abfcd.sh
	
	mkdir -p $(DESTDIR)$(ETCDIR)/abf/mock-urpm/configs/
	cp configs/* $(DESTDIR)$(ETCDIR)/abf/mock-urpm/configs/
	mkdir -p $(DESTDIR)/var/cache/abf/mock-urpm
	mkdir -p $(DESTDIR)/var/lib/abf/mock-urpm/src
	chmod 0777 $(DESTDIR)/var/lib/abf/mock-urpm -R
	
	

